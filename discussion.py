# [SECTION COMMENT]
# Comments in Python are done using "ctrl+/" or # symbol

# [Section] Python Syntax
# Hello World in Python
print("Hello World!")

# [SECTION] Indentation
# Where in other programming languages the indentation in code is for readability only, the indentation in Python is very important
# In Python, indentation is usedto indicate a blcok of code.
# Similar to JS, there is no need to end statements with semicolons

# [Section] Variables
# Variables are the container of data
# In python, a variable is declared by dictating the variable name and assigning a value using the equality symbol

# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers should begin with a letter (A to Z or a to z), dollar sign or an underscore
# after the first character, identifier can have any combination of characters
# Unlike JavaScript that uses the camel casing, Python uses snake case convention for variables as defined in the PEP (Python enhancement proposal)
# Most importantly, identifiers are case sensitive
age = 35
middle_initial = "C"
# Python allows assigning values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name4)

# [SECTION] Data types
# Data types convey what kind of information a variable holds. There are different data types and each has its own use.
# In Python, there are the commonly used data types:
# 1. Strings (str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers(int, float, complex) - for integers, decimal and complex numbers
num_of_days = 365 #This is an interger
pi_approx = 3.1416 #This is a float
complex_num = 1 + 5j #This is a complex number, letter j represents the imaginary number

print(type(complex_num))

# 3. Boolean(bool) - truth values
# Boolean values in Python start with uppercase letters
isLearning = True
isDifiicult = False

# [Section] Using Variables
# Just like in JS variables are used by simply calling the name of the identifier

print("My name is "+ full_name)
print(full_name + "" + secret_code)

# [Section] Terminal Outputs
# In python, printinh in the terminal uses  print() function
# To use variables, concatenate (+symbol) between strings can be used
# However, we cannot concatenate string ot a number or to a different data type
# print("My age is " + age)

# [Section] Typecasting
# here are some functions that can be used in typecasting
#1. int() - converts the value into an integer
print(int(3.75))
#2 float() - converts the value into an integer value
print(float(5))
#3 str() - converts teh value into string
print("my age is " + str(age))

# Another way to avoid type error in printing without the use of typecasting
# f-strings
print(f"Hi my name is {full_name} and my age is {age}.")

# [Section] Operations
# Python has operator families that can be used to manipulate variables

# Arithmetic Operator - performs mathematical operations

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)

# Assignment operators - used to assign to variables

num1 = 4

num1 += 3
print(num1)

# Other operators -=, +=, /=, %=

# Comparison operators - used to compare values (boolean values)

print( 1 == "1")

# Other operatos, !=, >=, <=, >, <

# Logical operators - used to combin conditional statements
# Logical Or operator
print(True or False)
#Logical And Operator
print(True and False)
# Logical Not Operator
print(not False)
