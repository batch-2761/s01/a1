name = "Evon Mondano"
age = 31
occupation = "React Developer"
movie = "Avengers: End game"
rating = 9.5

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}.")

num1, num2, num3 = 1, 2, 3

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)